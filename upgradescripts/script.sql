USE [master]
GO
/****** Object:  Database [ibrahimekinci]    Script Date: 23.05.2017 19:56:31 ******/
CREATE DATABASE [ibrahimekinci]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ibrahimekinci', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\ibrahimekinci.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ibrahimekinci_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\ibrahimekinci_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [ibrahimekinci] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ibrahimekinci].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ibrahimekinci] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ibrahimekinci] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ibrahimekinci] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ibrahimekinci] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ibrahimekinci] SET ARITHABORT OFF 
GO
ALTER DATABASE [ibrahimekinci] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ibrahimekinci] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ibrahimekinci] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ibrahimekinci] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ibrahimekinci] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ibrahimekinci] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ibrahimekinci] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ibrahimekinci] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ibrahimekinci] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ibrahimekinci] SET  ENABLE_BROKER 
GO
ALTER DATABASE [ibrahimekinci] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ibrahimekinci] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ibrahimekinci] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ibrahimekinci] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ibrahimekinci] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ibrahimekinci] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [ibrahimekinci] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ibrahimekinci] SET RECOVERY FULL 
GO
ALTER DATABASE [ibrahimekinci] SET  MULTI_USER 
GO
ALTER DATABASE [ibrahimekinci] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ibrahimekinci] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ibrahimekinci] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ibrahimekinci] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ibrahimekinci] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ibrahimekinci] SET QUERY_STORE = OFF
GO
USE [ibrahimekinci]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [ibrahimekinci]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ActivityLogs]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityLogs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Comment] [nvarchar](300) NOT NULL,
	[IpAddress] [nvarchar](50) NOT NULL,
	[ActivityLogTypeId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.ActivityLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ActivityLogTypes]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityLogTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.ActivityLogTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AddressBranchs]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddressBranchs](
	[Id] [int] NOT NULL,
	[BranchId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.AddressBranchs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Addresses]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Addresses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Detail] [nvarchar](300) NULL,
	[Country] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[County] [nvarchar](max) NULL,
	[Area] [nvarchar](max) NULL,
	[Neighborhood] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Addresses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AddressUsers]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddressUsers](
	[Id] [int] NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AddressUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[FirstName] [nvarchar](30) NULL,
	[LastName] [nvarchar](30) NULL,
	[Birthday] [datetime] NULL,
	[Gender] [bit] NULL,
	[LanguageId] [int] NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Branches]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Branches](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Detail] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Email] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](25) NULL,
	[Discriminator] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.Branches] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CommentGuests]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommentGuests](
	[Id] [int] NOT NULL,
	[FirstName] [nvarchar](30) NOT NULL,
	[LastName] [nvarchar](30) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[SiteUrl] [nvarchar](255) NULL,
 CONSTRAINT [PK_dbo.CommentGuests] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Comments]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Detail] [nvarchar](max) NOT NULL,
	[PostId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Comments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CommentUsers]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommentUsers](
	[Id] [int] NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.CommentUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ContactForms]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactForms](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](30) NOT NULL,
	[LastName] [nvarchar](30) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[SiteUrl] [nvarchar](255) NULL,
	[Subject] [nvarchar](50) NOT NULL,
	[Detail] [nvarchar](1000) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Discriminator] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.ContactForms] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Countries]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Countries](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Currency] [nvarchar](150) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Countries] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Languages]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Languages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[BinaryCode] [nvarchar](2) NOT NULL,
	[FilePath] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Languages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Logs]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Logs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
	[Comment] [nvarchar](300) NOT NULL,
	[IpAddress] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Logs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Media]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Media](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MainName] [nvarchar](200) NOT NULL,
	[FilePath] [nvarchar](255) NOT NULL,
	[MediaTypeId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Media] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MediaBagItems]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MediaBagItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DisplayUrl] [nvarchar](max) NOT NULL,
	[MediaId] [int] NOT NULL,
	[MediaBagId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.MediaBagItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MediaBagLocalizations]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MediaBagLocalizations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](150) NOT NULL,
	[Description] [nvarchar](300) NULL,
	[Keyword] [nvarchar](200) NULL,
	[DisplayUrl] [nvarchar](100) NOT NULL,
	[MediaBagId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.MediaBagLocalizations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MediaBags]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MediaBags](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MainName] [nvarchar](max) NOT NULL,
	[MainUrl] [nvarchar](max) NOT NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.MediaBags] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MediaLocalizations]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MediaLocalizations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Keyword] [nvarchar](200) NULL,
	[MediaId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.MediaLocalizations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MediaTypes]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MediaTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[DisplayUrl] [nvarchar](30) NOT NULL,
	[FileExtensions] [nvarchar](30) NOT NULL,
	[MinFileSize] [float] NOT NULL,
	[MaxFileSize] [float] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.MediaTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MenuItems]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[DisplayUrl] [nvarchar](100) NOT NULL,
	[Row] [int] NOT NULL,
	[TopMenuItemId] [int] NOT NULL,
	[MenuLocalizationId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.MenuItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MenuLocalizations]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuLocalizations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[DisplayUrl] [nvarchar](200) NOT NULL,
	[MenuId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.MenuLocalizations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Menus]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MainName] [nvarchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Menus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MessageTemplateLocalizations]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MessageTemplateLocalizations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Detail] [nvarchar](max) NULL,
	[ShortDetail] [nvarchar](200) NULL,
	[Tags] [nvarchar](max) NULL,
	[MetaTitle] [nvarchar](max) NULL,
	[MetaDescription] [nvarchar](max) NULL,
	[MetaKeywords] [nvarchar](max) NULL,
	[MessageTemplateId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.MessageTemplateLocalizations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MessageTemplateLocalizationTypeEmails]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MessageTemplateLocalizationTypeEmails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Subject] [nvarchar](100) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[BccEmailAddresses] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.MessageTemplateLocalizationTypeEmails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MessageTemplates]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MessageTemplates](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MainName] [nvarchar](30) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.MessageTemplates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PostActivitys]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostActivitys](
	[Id] [int] NOT NULL,
	[Quota] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.PostActivitys] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PostAnnouncements]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostAnnouncements](
	[Id] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.PostAnnouncements] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PostLocalizations]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostLocalizations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Detail] [nvarchar](max) NULL,
	[ShortDetail] [nvarchar](200) NULL,
	[Tags] [nvarchar](max) NULL,
	[MetaTitle] [nvarchar](max) NULL,
	[MetaDescription] [nvarchar](max) NULL,
	[MetaKeywords] [nvarchar](max) NULL,
	[DisplayUrl] [nvarchar](100) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[PostId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.PostLocalizations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PostProducts]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostProducts](
	[Id] [int] NOT NULL,
	[Price] [float] NOT NULL,
	[Stock] [float] NOT NULL,
 CONSTRAINT [PK_dbo.PostProducts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Posts]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Posts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MainName] [nvarchar](30) NOT NULL,
	[CommentStatus] [bit] NOT NULL,
	[CommentCount] [int] NOT NULL,
	[PostTypeId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Posts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PostTypes]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.PostTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SiteSetings]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SiteSetings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](255) NULL,
	[Row] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.SiteSetings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SubscriberGuests]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriberGuests](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.SubscriberGuests] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SubscriberUsers]    Script Date: 23.05.2017 19:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriberUsers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.SubscriberUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [IX_ActivityLogTypeId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_ActivityLogTypeId] ON [dbo].[ActivityLogs]
(
	[ActivityLogTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_BranchId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_BranchId] ON [dbo].[AddressBranchs]
(
	[BranchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[AddressBranchs]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[AddressUsers]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AddressUsers]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleNameIndex]    Script Date: 23.05.2017 19:56:31 ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_LanguageId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_LanguageId] ON [dbo].[AspNetUsers]
(
	[LanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 23.05.2017 19:56:31 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[CommentGuests]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_PostId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_PostId] ON [dbo].[Comments]
(
	[PostId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[CommentUsers]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[CommentUsers]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_LanguageId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_LanguageId] ON [dbo].[Countries]
(
	[LanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Name]    Script Date: 23.05.2017 19:56:31 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Name] ON [dbo].[Countries]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MediaTypeId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_MediaTypeId] ON [dbo].[Media]
(
	[MediaTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MediaBagId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_MediaBagId] ON [dbo].[MediaBagItems]
(
	[MediaBagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MediaId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_MediaId] ON [dbo].[MediaBagItems]
(
	[MediaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_LanguageId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_LanguageId] ON [dbo].[MediaBagLocalizations]
(
	[LanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MediaBagId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_MediaBagId] ON [dbo].[MediaBagLocalizations]
(
	[MediaBagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_LanguageId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_LanguageId] ON [dbo].[MediaLocalizations]
(
	[LanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MediaId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_MediaId] ON [dbo].[MediaLocalizations]
(
	[MediaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MenuLocalizationId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_MenuLocalizationId] ON [dbo].[MenuItems]
(
	[MenuLocalizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TopMenuItemId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_TopMenuItemId] ON [dbo].[MenuItems]
(
	[TopMenuItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_LanguageId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_LanguageId] ON [dbo].[MenuLocalizations]
(
	[LanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MenuId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_MenuId] ON [dbo].[MenuLocalizations]
(
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_LanguageId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_LanguageId] ON [dbo].[MessageTemplateLocalizations]
(
	[LanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MessageTemplateId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_MessageTemplateId] ON [dbo].[MessageTemplateLocalizations]
(
	[MessageTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[PostActivitys]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[PostAnnouncements]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_LanguageId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_LanguageId] ON [dbo].[PostLocalizations]
(
	[LanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_PostId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_PostId] ON [dbo].[PostLocalizations]
(
	[PostId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[PostProducts]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_PostTypeId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_PostTypeId] ON [dbo].[Posts]
(
	[PostTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 23.05.2017 19:56:31 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[SubscriberUsers]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActivityLogs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ActivityLogs_dbo.ActivityLogTypes_ActivityLogTypeId] FOREIGN KEY([ActivityLogTypeId])
REFERENCES [dbo].[ActivityLogTypes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ActivityLogs] CHECK CONSTRAINT [FK_dbo.ActivityLogs_dbo.ActivityLogTypes_ActivityLogTypeId]
GO
ALTER TABLE [dbo].[AddressBranchs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AddressBranchs_dbo.Addresses_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[Addresses] ([Id])
GO
ALTER TABLE [dbo].[AddressBranchs] CHECK CONSTRAINT [FK_dbo.AddressBranchs_dbo.Addresses_Id]
GO
ALTER TABLE [dbo].[AddressBranchs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AddressBranchs_dbo.Branches_BranchId] FOREIGN KEY([BranchId])
REFERENCES [dbo].[Branches] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AddressBranchs] CHECK CONSTRAINT [FK_dbo.AddressBranchs_dbo.Branches_BranchId]
GO
ALTER TABLE [dbo].[AddressUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AddressUsers_dbo.Addresses_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[Addresses] ([Id])
GO
ALTER TABLE [dbo].[AddressUsers] CHECK CONSTRAINT [FK_dbo.AddressUsers_dbo.Addresses_Id]
GO
ALTER TABLE [dbo].[AddressUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AddressUsers_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AddressUsers] CHECK CONSTRAINT [FK_dbo.AddressUsers_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUsers_dbo.Languages_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Languages] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUsers] CHECK CONSTRAINT [FK_dbo.AspNetUsers_dbo.Languages_LanguageId]
GO
ALTER TABLE [dbo].[CommentGuests]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CommentGuests_dbo.Comments_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[Comments] ([Id])
GO
ALTER TABLE [dbo].[CommentGuests] CHECK CONSTRAINT [FK_dbo.CommentGuests_dbo.Comments_Id]
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Comments_dbo.Posts_PostId] FOREIGN KEY([PostId])
REFERENCES [dbo].[Posts] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Comments] CHECK CONSTRAINT [FK_dbo.Comments_dbo.Posts_PostId]
GO
ALTER TABLE [dbo].[CommentUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CommentUsers_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CommentUsers] CHECK CONSTRAINT [FK_dbo.CommentUsers_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[CommentUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CommentUsers_dbo.Comments_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[Comments] ([Id])
GO
ALTER TABLE [dbo].[CommentUsers] CHECK CONSTRAINT [FK_dbo.CommentUsers_dbo.Comments_Id]
GO
ALTER TABLE [dbo].[Countries]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Countries_dbo.Languages_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Languages] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Countries] CHECK CONSTRAINT [FK_dbo.Countries_dbo.Languages_LanguageId]
GO
ALTER TABLE [dbo].[Media]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Media_dbo.MediaTypes_MediaTypeId] FOREIGN KEY([MediaTypeId])
REFERENCES [dbo].[MediaTypes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Media] CHECK CONSTRAINT [FK_dbo.Media_dbo.MediaTypes_MediaTypeId]
GO
ALTER TABLE [dbo].[MediaBagItems]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MediaBagItems_dbo.Media_MediaId] FOREIGN KEY([MediaId])
REFERENCES [dbo].[Media] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MediaBagItems] CHECK CONSTRAINT [FK_dbo.MediaBagItems_dbo.Media_MediaId]
GO
ALTER TABLE [dbo].[MediaBagItems]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MediaBagItems_dbo.MediaBags_MediaBagId] FOREIGN KEY([MediaBagId])
REFERENCES [dbo].[MediaBags] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MediaBagItems] CHECK CONSTRAINT [FK_dbo.MediaBagItems_dbo.MediaBags_MediaBagId]
GO
ALTER TABLE [dbo].[MediaBagLocalizations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MediaBagLocalizations_dbo.Languages_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Languages] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MediaBagLocalizations] CHECK CONSTRAINT [FK_dbo.MediaBagLocalizations_dbo.Languages_LanguageId]
GO
ALTER TABLE [dbo].[MediaBagLocalizations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MediaBagLocalizations_dbo.MediaBags_MediaBagId] FOREIGN KEY([MediaBagId])
REFERENCES [dbo].[MediaBags] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MediaBagLocalizations] CHECK CONSTRAINT [FK_dbo.MediaBagLocalizations_dbo.MediaBags_MediaBagId]
GO
ALTER TABLE [dbo].[MediaLocalizations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MediaLocalizations_dbo.Languages_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Languages] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MediaLocalizations] CHECK CONSTRAINT [FK_dbo.MediaLocalizations_dbo.Languages_LanguageId]
GO
ALTER TABLE [dbo].[MediaLocalizations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MediaLocalizations_dbo.Media_MediaId] FOREIGN KEY([MediaId])
REFERENCES [dbo].[Media] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MediaLocalizations] CHECK CONSTRAINT [FK_dbo.MediaLocalizations_dbo.Media_MediaId]
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MenuItems_dbo.MenuItems_TopMenuItemId] FOREIGN KEY([TopMenuItemId])
REFERENCES [dbo].[MenuItems] ([Id])
GO
ALTER TABLE [dbo].[MenuItems] CHECK CONSTRAINT [FK_dbo.MenuItems_dbo.MenuItems_TopMenuItemId]
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MenuItems_dbo.MenuLocalizations_MenuLocalizationId] FOREIGN KEY([MenuLocalizationId])
REFERENCES [dbo].[MenuLocalizations] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MenuItems] CHECK CONSTRAINT [FK_dbo.MenuItems_dbo.MenuLocalizations_MenuLocalizationId]
GO
ALTER TABLE [dbo].[MenuLocalizations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MenuLocalizations_dbo.Languages_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Languages] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MenuLocalizations] CHECK CONSTRAINT [FK_dbo.MenuLocalizations_dbo.Languages_LanguageId]
GO
ALTER TABLE [dbo].[MenuLocalizations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MenuLocalizations_dbo.Menus_MenuId] FOREIGN KEY([MenuId])
REFERENCES [dbo].[Menus] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MenuLocalizations] CHECK CONSTRAINT [FK_dbo.MenuLocalizations_dbo.Menus_MenuId]
GO
ALTER TABLE [dbo].[MessageTemplateLocalizations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MessageTemplateLocalizations_dbo.Languages_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Languages] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MessageTemplateLocalizations] CHECK CONSTRAINT [FK_dbo.MessageTemplateLocalizations_dbo.Languages_LanguageId]
GO
ALTER TABLE [dbo].[MessageTemplateLocalizations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MessageTemplateLocalizations_dbo.MessageTemplates_MessageTemplateId] FOREIGN KEY([MessageTemplateId])
REFERENCES [dbo].[MessageTemplates] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MessageTemplateLocalizations] CHECK CONSTRAINT [FK_dbo.MessageTemplateLocalizations_dbo.MessageTemplates_MessageTemplateId]
GO
ALTER TABLE [dbo].[PostActivitys]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PostActivitys_dbo.PostLocalizations_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[PostLocalizations] ([Id])
GO
ALTER TABLE [dbo].[PostActivitys] CHECK CONSTRAINT [FK_dbo.PostActivitys_dbo.PostLocalizations_Id]
GO
ALTER TABLE [dbo].[PostAnnouncements]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PostAnnouncements_dbo.PostLocalizations_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[PostLocalizations] ([Id])
GO
ALTER TABLE [dbo].[PostAnnouncements] CHECK CONSTRAINT [FK_dbo.PostAnnouncements_dbo.PostLocalizations_Id]
GO
ALTER TABLE [dbo].[PostLocalizations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PostLocalizations_dbo.Languages_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Languages] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PostLocalizations] CHECK CONSTRAINT [FK_dbo.PostLocalizations_dbo.Languages_LanguageId]
GO
ALTER TABLE [dbo].[PostLocalizations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PostLocalizations_dbo.Posts_PostId] FOREIGN KEY([PostId])
REFERENCES [dbo].[Posts] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PostLocalizations] CHECK CONSTRAINT [FK_dbo.PostLocalizations_dbo.Posts_PostId]
GO
ALTER TABLE [dbo].[PostProducts]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PostProducts_dbo.PostLocalizations_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[PostLocalizations] ([Id])
GO
ALTER TABLE [dbo].[PostProducts] CHECK CONSTRAINT [FK_dbo.PostProducts_dbo.PostLocalizations_Id]
GO
ALTER TABLE [dbo].[Posts]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Posts_dbo.PostTypes_PostTypeId] FOREIGN KEY([PostTypeId])
REFERENCES [dbo].[PostTypes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Posts] CHECK CONSTRAINT [FK_dbo.Posts_dbo.PostTypes_PostTypeId]
GO
ALTER TABLE [dbo].[SubscriberUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SubscriberUsers_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SubscriberUsers] CHECK CONSTRAINT [FK_dbo.SubscriberUsers_dbo.AspNetUsers_UserId]
GO
USE [master]
GO
ALTER DATABASE [ibrahimekinci] SET  READ_WRITE 
GO
