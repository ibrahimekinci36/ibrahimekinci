﻿using System.ComponentModel.DataAnnotations;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Plugin.CustomAttributes
{
    public class CustomRequiredAttribute : RequiredAttribute
    {
        public CustomRequiredAttribute()
        {
            ErrorMessageResourceType = typeof(Lang);
            ErrorMessageResourceName = "Warning_FieldRequired";
        }
    }
}
