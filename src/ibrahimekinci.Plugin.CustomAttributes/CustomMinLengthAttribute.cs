﻿using System.ComponentModel.DataAnnotations;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Plugin.CustomAttributes
{
    public class CustomMinLengthAttribute : MinLengthAttribute
    {
        public CustomMinLengthAttribute(int length) : base(length)
        {
            ErrorMessageResourceType = typeof(Lang);
            ErrorMessageResourceName = "Warning_MinLenght";
        }
    }
}
