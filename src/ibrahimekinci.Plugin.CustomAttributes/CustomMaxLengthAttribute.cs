﻿using System.ComponentModel.DataAnnotations;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Plugin.CustomAttributes
{
    public class CustomMaxLengthAttribute : MaxLengthAttribute
    {
        public CustomMaxLengthAttribute(int length) : base(length)
        {
            ErrorMessageResourceType = typeof(Lang);
            ErrorMessageResourceName = "Warning_MaxLenght";
        }
    }
}
