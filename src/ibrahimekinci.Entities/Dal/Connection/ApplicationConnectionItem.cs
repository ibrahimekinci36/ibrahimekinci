﻿namespace ibrahimekinci.Entities.Dal.Connection
{
    public class ApplicationConnectionItem
    {
        public string Scope { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public bool Status { get; set; }
    }
}
