﻿using System.Collections.Generic;
using System.Linq;

namespace ibrahimekinci.Entities.Dal.Connection
{
    public class ApplicationConnection
    {
        private static readonly List<ApplicationConnectionItem> ConnectionItems = new List<ApplicationConnectionItem>();
        private const string DefaultScope = "local";
        private const string DefaultKey = "mssql";
        private static ApplicationConnectionItem _defaultConnection;
        public static ApplicationConnectionItem DefaultConnection
        {
            get
            {
                if (_defaultConnection != null) return _defaultConnection;
                if (ConnectionItems.Count == 0)
                    ApplicationConnectionListFill();
                _defaultConnection = ConnectionItems.FirstOrDefault(x => x.Scope == DefaultScope && x.Key == DefaultKey1 && x.Status) ?? ConnectionItems[0];
                return _defaultConnection;
            }

        }

        public static string DefaultKey1 => DefaultKey;

        public static void ApplicationConnectionListFill()
        {
            ConnectionItems.Add(new ApplicationConnectionItem { Scope = "file", Key = "mdf", Value = "DbConnectionFileMdf", Status = true });
            ConnectionItems.Add(new ApplicationConnectionItem { Scope = "file", Key = "sdf", Value = "DbConnectionFileSdf", Status = true });
            ConnectionItems.Add(new ApplicationConnectionItem { Scope = "local", Key = "mssql", Value = "DbConnectionLocalMssql", Status = true });
            ConnectionItems.Add(new ApplicationConnectionItem { Scope = "local", Key = "mysql", Value = "DbConnectionLocalMysql", Status = false });
            ConnectionItems.Add(new ApplicationConnectionItem { Scope = "local", Key = "oracle", Value = "DbConnectionLocalOracle", Status = false });
            ConnectionItems.Add(new ApplicationConnectionItem { Scope = "server", Key = "mssql", Value = "DbConnectionServerMssql", Status = true });
            ConnectionItems.Add(new ApplicationConnectionItem { Scope = "server", Key = "mysql", Value = "DbConnectionServerMysql", Status = false });
            ConnectionItems.Add(new ApplicationConnectionItem { Scope = "server", Key = "oracle", Value = "DbConnectionServerOracle", Status = false });
            ConnectionItems.Add(new ApplicationConnectionItem { Scope = "azure", Key = "mssql", Value = "DbConnectionAzureMssql", Status = true });
            ConnectionItems.Add(new ApplicationConnectionItem { Scope = "azure", Key = "mysql", Value = "DbConnectionAzureMysql", Status = false });
            ConnectionItems.Add(new ApplicationConnectionItem { Scope = "azure", Key = "oracle", Value = "DbConnectionAzureOracle", Status = false });
        }
    }
}
