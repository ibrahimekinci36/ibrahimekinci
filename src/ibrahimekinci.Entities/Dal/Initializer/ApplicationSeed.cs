﻿using System.Data.Entity.Migrations;
using System.Linq;
using ibrahimekinci.Entities.Dal.Context;
using ibrahimekinci.Entities.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ibrahimekinci.Entities.Dal.Initializer
{
    public class AppSeed
    {
        private readonly ApplicationDbContext _db;
        public AppSeed(ApplicationDbContext db)
        {
            this._db = db;
            //veriler bir kere eklendikten sonra eklenmiyor
            if (db.Users.Any()) return;
            UserSeed();
            BasicSeed();
        }

        private static void UserSeed()
        {
            var manager = new UserManager<ApplicationUser>(
                new UserStore<ApplicationUser>(
                    new ApplicationDbContext()));
            var user = new ApplicationUser()
            {
                UserName = "ibrahimekinci36@gmail.com",
                Email = "ibrahimekinci36@gmail.com",
                PhoneNumberConfirmed = false,
                TwoFactorEnabled = false,
                AccessFailedCount = 0,
                EmailConfirmed = true,
                LockoutEnabled = false
            };
            manager.Create(user, "123456");
        }
        private void BasicSeed()
        {
            _db.Language.AddOrUpdate(
               new Language { Name = "tr", BinaryCode = "TR", FilePath = "/" },
                new Language { Name = "en", BinaryCode = "EN", FilePath = "/" },
                  new Language { Name = "it", BinaryCode = "IT", FilePath = "/" }
              );
            _db.SaveChanges();


            _db.Country.AddOrUpdate(
                new Country { Name = "Türkiye", LanguageId = 1, Currency = "TL" },
                new Country { Name = "İtalya", LanguageId = 1, Currency = "Auro" },
                new Country { Name = "Amerika", LanguageId = 1, Currency = "Dolar" },
                new Country { Name = "ingiltere", LanguageId = 1, Currency = "Sterlin" }
            );

            _db.PostType.AddOrUpdate(
             new PostType { Name = "Yazı" }
           , new PostType { Name = "Sayfa" }
           , new PostType { Name = "Etkinlik" }
           , new PostType { Name = "Duyuru" }
           , new PostType { Name = "Ürün" }
           );


            _db.ContactForm.AddOrUpdate(
               new ContactForm { FirstName = "ibrahim", LastName = "Ekinci", Email = "ibrahimekinci36@gmail.com", SiteUrl = "http://www.ibrahimekinci.com.tr/", Subject = "Yeni web sitesi", Detail = "Form Detay 1", Status = true }
               , new ContactForm { FirstName = "Aras", LastName = "Ekinci", Email = "info@arasekinci.com.tr", SiteUrl = "http://www.arasekinci.com.tr/", Subject = "Konu Test", Detail = "Detay2", Status = true }
                //new ContactForm { FirstName = "", LastName = "", Email = "", SiteUrl = "", Subject = "", CreateDate = DateTime.Now, Status = true }
                );

            _db.Branch.AddOrUpdate(
                new Branch { Name = "İstanbul Şube", Status = true }
                , new Branch { Name = "Aydın Şube", Status = true }
                );


            _db.SiteSetings.AddOrUpdate(
             new SiteSetings { Name = "SiteName", Value = "ibrahimekinci", Row = -1 }
             , new SiteSetings { Name = "SiteLanguage", Value = "2", Row = 0 }
             , new SiteSetings { Name = "SiteUrl", Value = "www.ibrahimekinci.com.tr", Row = 1 }
             , new SiteSetings { Name = "SiteOwner", Value = "İBRAHİM EKİNCİ", Row = 2 }
             , new SiteSetings { Name = "SiteTitle", Value = "İçerik Yönetim Sistemi", Row = 3 }
             , new SiteSetings { Name = "SiteDescription", Value = "", Row = 4 }
             , new SiteSetings { Name = "SiteCopy", Value = "", Row = 5 }
             , new SiteSetings { Name = "SiteLogo", Value = "", Row = 6 }
             , new SiteSetings { Name = "SiteFaviconUrl", Value = "", Row = 7 }
             , new SiteSetings { Name = "MailServer", Value = "", Row = 8 }
             , new SiteSetings { Name = "MailUser", Value = "", Row = 9 }
             , new SiteSetings { Name = "MailPassword", Value = "", Row = 10 }
             , new SiteSetings { Name = "MailPort", Value = "", Row = 11 }
             , new SiteSetings { Name = "ImageExtension", Value = "", Row = 12 }
             , new SiteSetings { Name = "VideoExtension", Value = "", Row = 13 }
             , new SiteSetings { Name = "FileExtension", Value = "", Row = 14 }
             , new SiteSetings { Name = "FacebookAppId", Value = "", Row = 15 }
             , new SiteSetings { Name = "FacebookAppSecret", Value = "", Row = 16 }
             , new SiteSetings { Name = "GoogleMeta", Value = "", Row = 17 }
             , new SiteSetings { Name = "GoogleAnalytics", Value = "", Row = 18 }
             );

            _db.SaveChanges();
        }

    }
}
