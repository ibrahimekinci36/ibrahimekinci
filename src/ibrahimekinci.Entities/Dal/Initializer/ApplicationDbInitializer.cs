﻿using System.Data.Entity;
using ibrahimekinci.Entities.Dal.Context;

namespace ibrahimekinci.Entities.Dal.Initializer
{
    public class ApplicationDbInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        public override void InitializeDatabase(ApplicationDbContext db)
        {
            base.InitializeDatabase(db);
        }
        protected override void Seed(ApplicationDbContext db)
        {
            var appSeed = new AppSeed(db);
            base.Seed(db);
        }
    }
}