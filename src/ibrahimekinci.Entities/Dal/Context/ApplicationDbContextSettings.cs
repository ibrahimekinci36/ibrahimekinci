﻿using System.Data.Entity;
using System.Linq;
using ibrahimekinci.Entities.Dal.Connection;
using ibrahimekinci.Entities.Dal.Initializer;
using ibrahimekinci.Entities.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ibrahimekinci.Entities.Dal.Context
{
    public class ApplicationDbContextSettings : IdentityDbContext<ApplicationUser>
    {
        static ApplicationDbContextSettings()
        {
            //using (var context = new ApplicationDbContext())
            //{
            //    context.Database.Delete();
            //    context.Database.Create();
            //}
        }
        //identity Başla
        public ApplicationDbContextSettings() : base(ApplicationConnection.DefaultConnection.Value, throwIfV1Schema: false)
        {
            // Database.SetInitializer(new DropCreateDatabaseAlways<ApplicationDbContext>());
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ApplicationDbContext>());
            Database.SetInitializer(new ApplicationDbInitializer());
        }

        public static ApplicationDbContext Create() { return new ApplicationDbContext(); }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AddressBranch>().ToTable("AddressBranchs");
            modelBuilder.Entity<AddressUser>().ToTable("AddressUsers");

            modelBuilder.Entity<CommentUser>().ToTable("CommentUsers");
            modelBuilder.Entity<CommentGuest>().ToTable("CommentGuests");

            //modelBuilder.Entity<MediaGif>().ToTable("MediaGifs");
            //modelBuilder.Entity<MediaImage>().ToTable("MediaImageses");
            //modelBuilder.Entity<MediaSound>().ToTable("MediaSounds");
            //modelBuilder.Entity<MediaVideo>().ToTable("MediaVideos");

            //modelBuilder.Entity<MessageTemplateLocalizationTypeEmail>().ToTable("MessageTemplateLocalizationTypeEmails");

            modelBuilder.Entity<PostActivity>().ToTable("PostActivitys");
            modelBuilder.Entity<PostAnnouncement>().ToTable("PostAnnouncements");
            modelBuilder.Entity<PostProduct>().ToTable("PostProducts");


            base.OnModelCreating(modelBuilder);
        }
        //veri tabanında oluşturulan tabloların sonuna otomatik gelen "s" takısının eklenmesini engeller
        //  protected override void OnModelCreating(DbModelBuilder modelBuilder) { modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.PluralizingTableNameConvention>(); }
        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                .SelectMany(x => x.ValidationErrors)
                .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new System.Data.Entity.Validation.DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }
        /////save 
    }
}
