﻿using System.Data.Entity;
using ibrahimekinci.Entities.Models;

namespace ibrahimekinci.Entities.Dal.Context
{
    public class ApplicationDbContext : ApplicationDbContextSettings
    {
        public DbSet<Address> Address { get; set; }
        //public DbSet<AddressBranch> AddressBranch { get; set; }
        //public DbSet<AddressPersonal> AddressPersonal { get; set; }
        public DbSet<Branch> Branch { get; set; }
        public DbSet<Comment> Comment { get; set; }
        //public DbSet<CommentUser> CommentUser { get; set; }
        //public DbSet<CommentGuest> CommentGuest { get; set; }
        public DbSet<ContactForm> ContactForm { get; set; }
        public DbSet<Language> Language { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<ActivityLog> ActivityLog { get; set; }
        public DbSet<ActivityLogType> ActivityLogType { get; set; }
        public DbSet<Log> Log { get; set; }
        public DbSet<Media> Media { get; set; }
        public DbSet<MediaLocalization> MediaLocalization { get; set; }
        public DbSet<MediaType> MediaType { get; set; }
        //public DbSet<MediaGif> MediaGif { get; set; }
        //public DbSet<MediaImage> MediaImage { get; set; }
        //public DbSet<MediaSound> MediaSound { get; set; }
        //public DbSet<MediaVideo> MediaVideo { get; set; }
        public DbSet<MediaBag> MediaBag { get; set; }
        public DbSet<MediaBagLocalization> MediaBagLocalization { get; set; }
        public DbSet<MediaBagItem> MediaBagItem { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<MenuItem> MenuItem { get; set; }
        public DbSet<MenuLocalization> MenuLocalization { get; set; }
        public DbSet<MessageTemplate> MessageTemplate { get; set; }
        public DbSet<MessageTemplateLocalization> MessageTemplateLocalization { get; set; }
        public DbSet<MessageTemplateLocalizationTypeEmail> MessageTemplateLocalizationTypeEmail { get; set; }
        public DbSet<Post> Post { get; set; }
        public DbSet<PostLocalization> PostLocalization { get; set; }
        public DbSet<PostType> PostType { get; set; }
        //public DbSet<PostActivity> PostActivity { get; set; }
        //public DbSet<PostAnnouncement> PostAnnouncement { get; set; }
        //public DbSet<PostProduct> PostProduct { get; set; }
        public DbSet<SiteSetings> SiteSetings { get; set; }
        public DbSet<SubscriberGuest> SubscriberGuest { get; set; }
        public DbSet<SubscriberUser> SubscriberUser { get; set; }
        //public DbSet<ApplicationAdmin> ApplicationAdmin { get; set; }
    }
}
