using ibrahimekinci.Entities.Dal;
using System.Data.Entity.Migrations;
using System.Linq;
using ibrahimekinci.Entities.Dal.Context;
using ibrahimekinci.Entities.Dal.Initializer;
using ibrahimekinci.Entities.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ibrahimekinci.Entities.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {

            AutomaticMigrationsEnabled = true;//Fasle iken true yap�ld�.S�rekli migration ekleme i�ini otomatik yapmak i�in 
            AutomaticMigrationDataLossAllowed = false;
            //  ContextKey = "ibrahimekinci.Entities.Migrations.Configuration";//Context s�n�f�
            ContextKey = "ibrahimekinci.Entities.Dal.Context.ApplicationDbContext";//Context s�n�f�
        }

        protected override void Seed(ApplicationDbContext db)
        {
            //context.Database.Delete();
            //context.Database.Create();
            var appSeed = new AppSeed(db);
            base.Seed(db);

        }

    }
}
