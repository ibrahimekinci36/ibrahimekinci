﻿using ibrahimekinci.Plugin.Localization.LanguageResource;
using ibrahimekinci.Plugin.CustomAttributes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ibrahimekinci.Entities.Models
{
    [Table("PostActivity")]
    public class PostActivity: PostLocalization
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelQuota"), CustomRequired()]
        public int Quota { get; set; } = 0;
        [Display(ResourceType = typeof(Lang), Name = "LabelStartDate"), CustomRequired]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; } = DateTime.Now;
        [Display(ResourceType = typeof(Lang), Name = "LabelEndDate"), CustomRequired]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; } = DateTime.Now;
    }
}