﻿using ibrahimekinci.Plugin.CustomAttributes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    public class PostType:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelPostType"), CustomRequired(), CustomMaxLength(30)]
        public string Name { get; set; }

        public virtual ICollection<Post> Post { get; set; }
    }
}