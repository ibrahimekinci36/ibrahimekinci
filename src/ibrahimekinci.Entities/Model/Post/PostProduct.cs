﻿using ibrahimekinci.Plugin.Localization.LanguageResource;
using ibrahimekinci.Plugin.CustomAttributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ibrahimekinci.Entities.Models
{
    [Table("PostProducts")]
    public class PostProduct: PostLocalization
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelPrice"), CustomRequired]
        public double Price { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelStock"), CustomRequired]
        public double Stock { get; set; }
    }
}