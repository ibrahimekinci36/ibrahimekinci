﻿using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ibrahimekinci.Entities.Models
{
    public class PostLocalization:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelTitle"), CustomRequired]
        public string Title { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelDetail")]
        public string Detail { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelShortDetail"), CustomMaxLength(200)]
        public string ShortDetail { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMetaTitle")]
        public string Tags { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelTags")]
        public string MetaTitle { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMetaDescription")]
        public string MetaDescription { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMetaKeywords")]
        public string MetaKeywords { get; set; }

        [Display(ResourceType = typeof(Lang), Name = "LabelDisplayUrl"), CustomRequired, CustomMaxLength(100), CustomMinLength(1)]
        public string DisplayUrl { get; set; }

        [CustomRequired()]
        public int LanguageId { get; set; }
        [CustomRequired()]
        public int PostId { get; set; }


        [ForeignKey("LanguageId")]
        public virtual Language Language { get; set; }
        [ForeignKey("PostId")]
        public virtual Post Post { get; set; }
    }
}