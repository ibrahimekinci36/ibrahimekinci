﻿using ibrahimekinci.Plugin.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ibrahimekinci.Plugin.Localization.LanguageResource;
namespace ibrahimekinci.Entities.Models
{
    public class Post : BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelMainName"), CustomRequired, CustomMinLength(2), CustomMaxLength(30)]
        public string MainName { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelStatusComment"), CustomRequired]
        public bool CommentStatus { get; set; } = true;
        [Display(ResourceType = typeof(Lang), Name = "LabelCommentCount"), CustomRequired]
        public int CommentCount { get; set; } = 0;

        [Display(ResourceType = typeof(Lang), Name = "LabelPostType"), CustomRequired]
        public int PostTypeId { get; set; }

        [ForeignKey("PostTypeId")]
        public virtual PostType PostType { get; set; }
        public virtual ICollection<PostLocalization> PostLocalization { get; set; }
    }
}