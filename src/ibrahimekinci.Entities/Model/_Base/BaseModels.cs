﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
  public  abstract class BaseModels
    {
        [Key]
        [Display(ResourceType = typeof(Lang), Name = "LabelId"), CustomRequired]
        public int Id { get; set; }

        [Display(ResourceType = typeof(Lang), Name = "LabelCreateDate"), CustomRequired]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime CreatedDate { get; set; } = DateTime.Now;

        [Display(ResourceType = typeof(Lang), Name = "LabelModifedDate"), CustomRequired]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime ModifedDate { get; set; } = DateTime.Now;

        [Display(ResourceType = typeof(Lang), Name = "LabelStatus"), CustomRequired]
        public bool Status { get; set; } = true;

        [Display(ResourceType = typeof(Lang), Name = "LabelDeleted"), CustomRequired]
        public bool Deleted { get; set; } = false;


    }
}
