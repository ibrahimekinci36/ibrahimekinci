﻿using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;
using System.ComponentModel.DataAnnotations;

namespace ibrahimekinci.Entities.Models
{
    public class SiteSetings:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelName"), CustomRequired, CustomMaxLength(50)]
        public string Name { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelValue"), CustomMaxLength(255)]
        public string Value { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelRow"), CustomRequired]
        public int Row { get; set; }
    }
}