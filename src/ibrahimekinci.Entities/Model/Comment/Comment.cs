﻿using ibrahimekinci.Plugin.CustomAttributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    public abstract class Comment:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelDetail"), CustomRequired]
        public string Detail { get; set; }

        [Display(ResourceType = typeof(Lang), Name = "LabelPost"), CustomRequired]
        public int PostId { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelOwner"), CustomRequired]

        [ForeignKey("PostId")]
        public virtual Post Post { get; set; }
    }
}