﻿using ibrahimekinci.Plugin.CustomAttributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    [Table("CommentUsers")]
    public class CommentUser:Comment
    {
 
        [Display(ResourceType = typeof(Lang), Name = "LabelUser"), CustomRequired]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

    }
}