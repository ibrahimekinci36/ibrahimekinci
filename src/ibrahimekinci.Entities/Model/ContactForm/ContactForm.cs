﻿using ibrahimekinci.Plugin.CustomAttributes;
using System.ComponentModel.DataAnnotations;
using ibrahimekinci.Plugin.Localization.LanguageResource;
namespace ibrahimekinci.Entities.Models
{
    public class ContactForm:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelFirstName"), CustomRequired, CustomMinLength(2), CustomMaxLength(30)]
        public string FirstName { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelLastName"), CustomRequired, CustomMinLength(2), CustomMaxLength(30)]
        public string LastName { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelEmail"), CustomRequired(), CustomMaxLength(50), DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelSiteUrl"), CustomMaxLength(255), DataType(DataType.Url)]
        public string SiteUrl { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelSubject"), CustomRequired, CustomMinLength(2), CustomMaxLength(50)]
        public string Subject { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelDetail"), CustomRequired(), CustomMinLength(2), CustomMaxLength(1000)]
        public string Detail { get; set; }
    }
}