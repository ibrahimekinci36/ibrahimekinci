﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    public  class Media:BaseModels
    {
   
        [Display(ResourceType = typeof(Lang), Name = "LabelMainName"), CustomRequired,CustomMaxLength(200)]
        public string MainName { get; set; }

        [Display(ResourceType = typeof(Lang), Name = "LabelFilePath"), CustomRequired, CustomMaxLength(255)]
        public string FilePath { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMedia"), CustomRequired()]
        public int MediaTypeId { get; set; }
        [ForeignKey("MediaTypeId")]
        public virtual MediaType MediaType { get; set; }

        public virtual ICollection<MediaLocalization> MediaLocalization { get; set; }


    }
}