﻿using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ibrahimekinci.Entities.Models
{
    public class MediaLocalization: BaseModels
    {

        [Display(ResourceType = typeof(Lang), Name = "LabelKeyword"), CustomMaxLength(200)]
        public string Keyword { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMedia"), CustomRequired()]
        public int MediaId { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelLanguage"), CustomRequired()]
        public int LanguageId { get; set; }
        [ForeignKey("MediaId")]
        public virtual Media Media { get; set; }
        [ForeignKey("LanguageId")]
        public virtual Language Language{ get; set; }
    }
}