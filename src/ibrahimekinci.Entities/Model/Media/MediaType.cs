﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    public class MediaType:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelMediaType"), CustomRequired, CustomMaxLength(30), CustomMinLength(2)]
        public string Name { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelDisplayUrl"), CustomRequired, CustomMaxLength(30), CustomMinLength(2)]
        public string DisplayUrl { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMediaType"), CustomRequired, CustomMaxLength(30), CustomMinLength(2)]
        public string FileExtensions { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMinFileSize"), CustomRequired]
        public double MinFileSize { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMaxFileSize"), CustomRequired]
        public double MaxFileSize { get; set; }
    }
}
