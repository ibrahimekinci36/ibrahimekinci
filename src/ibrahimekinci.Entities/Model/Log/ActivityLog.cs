﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    public class ActivityLog:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelComment"), CustomRequired, CustomMinLength(2), CustomMaxLength(300)]
        public string Comment { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelIp"), CustomRequired, CustomMinLength(2), CustomMaxLength(50)]
        public string IpAddress { get; set; }

        public int ActivityLogTypeId { get; set; }
        [ForeignKey("ActivityLogTypeId")]

        public virtual ActivityLogType ActivityLogType { get; set; }
    }
}
