﻿using System;
using System.ComponentModel.DataAnnotations;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    public class Log:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelTitle"), CustomRequired, CustomMinLength(2), CustomMaxLength(100)]
        public string Title { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelComment"), CustomRequired, CustomMinLength(2), CustomMaxLength(300)]
        public string Comment { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelIp"), CustomRequired, CustomMinLength(2), CustomMaxLength(50)]
        public string IpAddress { get; set; }
    }
}
