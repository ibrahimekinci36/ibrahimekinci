﻿using System.ComponentModel.DataAnnotations;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    public class ActivityLogType:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelName"), CustomRequired, CustomMinLength(2), CustomMaxLength(50)]
        public string Name { get; set; }
    }
}
