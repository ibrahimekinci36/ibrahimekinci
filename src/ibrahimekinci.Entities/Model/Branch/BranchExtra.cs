﻿using ibrahimekinci.Plugin.CustomAttributes;
using System.ComponentModel.DataAnnotations;
using ibrahimekinci.Plugin.Localization.LanguageResource;
namespace ibrahimekinci.Entities.Models
{
    public class BranchExtra : Branch
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelEmail"), CustomMaxLength(50), DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelPhoneNumber"), CustomMinLength(5), CustomMaxLength(25), DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }


    }
}