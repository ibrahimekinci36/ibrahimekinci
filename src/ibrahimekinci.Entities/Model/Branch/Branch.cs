﻿using System.Collections.Generic;
using ibrahimekinci.Plugin.CustomAttributes;
using System.ComponentModel.DataAnnotations;
using ibrahimekinci.Plugin.Localization.LanguageResource;
namespace ibrahimekinci.Entities.Models
{
    public class Branch:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelBranchName"), CustomRequired, CustomMinLength(2), CustomMaxLength(50)]
        public string Name { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelDetail")]
        public string Detail { get; set; }
        public virtual ICollection<AddressBranch> Address { get; set; }
    }
}