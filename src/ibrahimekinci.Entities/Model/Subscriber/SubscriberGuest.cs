﻿using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;
using System.ComponentModel.DataAnnotations;

namespace ibrahimekinci.Entities.Models
{
    public class SubscriberGuest:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelEmail"), CustomRequired(), CustomMaxLength(50), DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}