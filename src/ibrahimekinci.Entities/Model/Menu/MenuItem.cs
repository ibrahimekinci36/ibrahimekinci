﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using ibrahimekinci.Plugin.Localization.LanguageResource;
using ibrahimekinci.Plugin.CustomAttributes;

namespace ibrahimekinci.Entities.Models
{
    public class MenuItem:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelName"), CustomRequired, CustomMaxLength(100)]
        public string Name { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelDisplayUrl"), CustomRequired, CustomMaxLength(100), CustomMinLength(1)]
        public string DisplayUrl { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelRow"), CustomRequired]
        public int Row { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMenuItem"), CustomRequired]
        public int TopMenuItemId { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMenuItem"), CustomRequired]
        public int MenuLocalizationId { get; set; }

        [ForeignKey("MenuLocalizationId")]
        public virtual MenuLocalization MenuLocalization { get; set; }
        [ForeignKey("TopMenuItemId")]
        public virtual MenuItem TopMenuItem { get; set; }
        public virtual ICollection<MenuItem> SubMenuItems { get; set; }

    }
}