﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using ibrahimekinci.Plugin.Localization.LanguageResource;
using ibrahimekinci.Plugin.CustomAttributes;

namespace ibrahimekinci.Entities.Models
{
    public class MenuLocalization:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelName"), CustomRequired, CustomMaxLength(100)]
        public string Name { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelDisplayUrl"), CustomMaxLength(200), CustomRequired]
        public string DisplayUrl { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMenu"), CustomRequired]
        public int MenuId { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelLanguage"), CustomRequired]
        public int LanguageId { get; set; }
        [ForeignKey("LanguageId")]
        public virtual Language Language { get; set; }
        [ForeignKey("MenuId")]
        public virtual Menu Menu { get; set; }
        public virtual ICollection<MenuItem> MenuItem { get; set; }
    }
}