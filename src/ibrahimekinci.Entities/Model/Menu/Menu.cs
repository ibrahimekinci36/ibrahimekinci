﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    public class Menu:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelMainName"), CustomRequired, CustomMaxLength(100)]
        public string MainName { get; set; }
        public virtual ICollection<MenuLocalization> MenuLocalization { get; set; }
    }
}