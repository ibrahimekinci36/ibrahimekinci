﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    public class Language:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelName"), CustomRequired, CustomMaxLength(100)]
        public string Name { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelBinaryCode"), CustomRequired, CustomLength(2)]
        public string BinaryCode { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelFilePath"), CustomRequired]
        public string FilePath { get; set; }
        public virtual ICollection<Country> Country { get; set; }
        public virtual ICollection<PostLocalization> PostLocalization { get; set; }
        public virtual ICollection<MenuLocalization> MenuLocalization { get; set; }
        public virtual ICollection<MediaLocalization> MediaLocalization { get; set; }
        public virtual ICollection<MediaBagLocalization> MediaBagLocalization { get; set; }
    }
}