﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    public class Country:BaseModels
    {
        [Index(IsUnique = true)]
        [Display(ResourceType = typeof(Lang), Name = "LabelCountry"), CustomRequired,CustomMaxLength(150)]
        public string Name { get; set; }

        [Display(ResourceType = typeof(Lang), Name = "LabelCurrency"), CustomRequired,CustomMaxLength(30)]
        public string Currency { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelLanguageResource"), CustomRequired]

        public int LanguageId { get; set; }
        [ForeignKey("LanguageId")]
        public virtual Language Language { get; set; }
    }
}