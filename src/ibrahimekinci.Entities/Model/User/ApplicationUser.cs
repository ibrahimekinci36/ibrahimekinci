﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ibrahimekinci.Entities.Models
{
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        [Display(ResourceType = typeof(Lang), Name = "LabelFirstName"), CustomMinLength(2), CustomMaxLength(30)]
        public string FirstName { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelLastName"), CustomMinLength(2), CustomMaxLength(30)]
        public string LastName { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelBirthday")]
        public DateTime? Birthday { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelGender")]
        public bool? Gender { get; set; }
        public int? LanguageId { get; set; }
        [ForeignKey("LanguageId")]
        public virtual Language Language { get; set; }
        public virtual ICollection<CommentUser> CommentUser { get; set; }
        public virtual ICollection<AddressUser> Address { get; set; }

    }
}