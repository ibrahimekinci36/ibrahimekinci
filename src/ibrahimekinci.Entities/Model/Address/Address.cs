﻿using System.ComponentModel.DataAnnotations;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    public abstract class Address : BaseModels,IAddress
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelAddressName"), CustomRequired, CustomMinLength(2), CustomMaxLength(50)]
        public string Name { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelDetail"), CustomMaxLength(300)]
        public string Detail { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelCountry")]
        public string Country { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelCity")]
        public string City { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelCounty")]
        public string County { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelArea")]
        public string Area { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelNeighborhood")]
        public string Neighborhood { get; set; }
    }
}