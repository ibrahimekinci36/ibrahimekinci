﻿namespace ibrahimekinci.Entities.Models
{
    internal interface IAddress
    {
        int Id { get; set; }
        string Name { get; set; }
        string Detail { get; set; }
        string Country { get; set; }

        string City { get; set; }

        string County { get; set; }

        string Area { get; set; }
        string Neighborhood { get; set; }
    }
}
