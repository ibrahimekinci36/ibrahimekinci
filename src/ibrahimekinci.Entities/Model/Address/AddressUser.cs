﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    [Table("AddressUsers")]
    public class AddressUser : Address
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelOwner"), CustomRequired]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
    }
}