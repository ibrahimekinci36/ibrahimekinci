﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;
namespace ibrahimekinci.Entities.Models
{
    public class MediaBag
    {
        [Key]
        public int Id { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMainName"), CustomRequired]
        public string MainName { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelDisplayUrl"), CustomRequired]
        public string MainUrl { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelStatus"), CustomRequired]
        public bool Status { get; set; }
        public virtual ICollection<MediaBagLocalization> MediaBagLocalization { get; set; }
        public virtual ICollection<MediaBagItem> MediaBagMedia { get; set; }
    }
}