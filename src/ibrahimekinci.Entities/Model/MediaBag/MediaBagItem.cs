﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;
namespace ibrahimekinci.Entities.Models
{
    public class MediaBagItem :BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelDisplayUrl"), CustomRequired]
        public string DisplayUrl { get; set; }

        [Display(ResourceType = typeof(Lang), Name = "LabelMedia"), CustomRequired]
        public int MediaId { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMediaBag"), CustomRequired]
        public int MediaBagId { get; set; }
     
        [ForeignKey("MediaId")]
        public virtual Media Media { get; set; }
        [ForeignKey("MediaBagId")]
        public virtual MediaBag MediaBag { get; set; }

    }
}