﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    public class MediaBagLocalization:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelTitle"), CustomRequired, CustomMaxLength(150)]
        public string Title { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelDescription"), CustomMaxLength(300)]
        public string Description { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelKeyword"), CustomMaxLength(200)]
        public string Keyword { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelDisplayUrl"), CustomRequired, CustomMaxLength(100), CustomMinLength(1)]
        public string DisplayUrl { get; set; }

        [Display(ResourceType = typeof(Lang), Name = "LabelMediaBag"), CustomRequired]
        public int MediaBagId { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelLanguage"), CustomRequired]
        public int LanguageId { get; set; }
        [ForeignKey("MediaBagId")]
        public virtual MediaBag MediaBag { get; set; }
        [ForeignKey("LanguageId")]
        public virtual Language Language{ get; set; }
    }
}