﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    public class MessageTemplate:BaseModels
    {

        [Display(ResourceType = typeof(Lang), Name = "LabelMainName"), CustomRequired, CustomMinLength(2), CustomMaxLength(30)]
        public string MainName { get; set; }
        public virtual ICollection<MessageTemplateLocalization> MessageTemplateLocalization { get; set; }
    }
}
