﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    public class MessageTemplateLocalization:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelTitle"), CustomRequired]
        public string Title { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelDetail")]
        public string Detail { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelShortDetail"), CustomMaxLength(200)]
        public string ShortDetail { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMetaTitle")]
        public string Tags { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelTags")]
        public string MetaTitle { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMetaDescription")]
        public string MetaDescription { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelMetaKeywords")]
        public string MetaKeywords { get; set; }

        [Display(ResourceType = typeof(Lang), Name = "LabelMenuItem"), CustomRequired]
        public int MessageTemplateId { get; set; }

        [ForeignKey("MessageTemplateId")]
        public virtual MessageTemplate MessageTemplate { get; set; }

        [CustomRequired()]
        public int LanguageId { get; set; }
        [ForeignKey("LanguageId")]
        public virtual Language Language { get; set; }
    }
}
