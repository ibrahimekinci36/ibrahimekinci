﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ibrahimekinci.Plugin.CustomAttributes;
using ibrahimekinci.Plugin.Localization.LanguageResource;

namespace ibrahimekinci.Entities.Models
{
    [Table("MessageTemplateLocalizationTypeEmails")]
    public class MessageTemplateLocalizationTypeEmail:BaseModels
    {
        [Display(ResourceType = typeof(Lang), Name = "LabelName"), CustomRequired,  CustomMaxLength(50)]
        public string Name { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelSubject"), CustomRequired,  CustomMaxLength(100)]
        public string Subject { get; set; }
        [Display(ResourceType = typeof(Lang), Name = "LabelBody"), CustomRequired]
        public string Body { get; set; }
        public string BccEmailAddresses { get; set; }
    }
}
