﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ibrahimekinci.Web.Startup))]
namespace ibrahimekinci.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
